import sys
import os

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon

from controller import AppController
from model import AppModel
from view import AppView


def main():
    app = QApplication(sys.argv)
    view = AppView()
    view.setWindowTitle("VideoSynchronizer")
    if hasattr(sys, '_MEIPASS'):
        view.setWindowIcon(QIcon(os.path.join(sys._MEIPASS, "icon.ico")))
    else:
        view.setWindowIcon(QIcon("icon.ico"))
    view.show()
    AppController(view=view, model=AppModel())
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
