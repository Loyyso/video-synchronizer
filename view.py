from PyQt5.QtCore import Qt
from PyQt5.QtMultimedia import QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QWidget, QPushButton, QStyle, QLabel, \
    QHBoxLayout, QLineEdit, QSpinBox, QSlider


class AppView(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Application")

        self.main_layout = QVBoxLayout()
        self._central_widget = QWidget(self)
        self.setCentralWidget(self._central_widget)
        self._central_widget.setLayout(self.main_layout)

        self._create_menu()
        self._create_videos()
        self._create_slider()

        self._create_offset_field()

        self.playButton = QPushButton()
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.main_layout.addWidget(self.playButton)

        self._create_video_selectors()
        self._create_render_button()

    def _create_videos(self):
        self.media_player_1 = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        video_widget_1 = QVideoWidget()
        video_widget_1.setMinimumSize(200, 200)
        self.main_layout.addWidget(video_widget_1)
        self.media_player_1.setVideoOutput(video_widget_1)

        self.media_player_2 = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        video_widget_2 = QVideoWidget()
        video_widget_2.setMinimumSize(200, 200)
        self.main_layout.addWidget(video_widget_2)
        self.media_player_2.setVideoOutput(video_widget_2)

    def _create_menu(self):
        self.menu = self.menuBar().addMenu("&Menu")
        self.menu.addAction('&Exit', self.close)

    def _create_video_selectors(self):
        self.file1_layout = QHBoxLayout()
        self.file1_label = QLabel("File 1: ")
        self.file1_layout.addWidget(self.file1_label, 10)
        self.open_button_1 = QPushButton("Select video")
        self.file1_layout.addWidget(self.open_button_1)
        self.main_layout.addLayout(self.file1_layout)

        self.file2_layout = QHBoxLayout()
        self.file2_label = QLabel("File 2: ")
        self.file2_layout.addWidget(self.file2_label, 10)
        self.open_button_2 = QPushButton("Select video")
        self.file2_layout.addWidget(self.open_button_2)
        self.main_layout.addLayout(self.file2_layout)

    def _create_offset_field(self):
        self.offset_layout = QHBoxLayout()
        self.offset_label = QLabel("Offset (ms) :")
        self.offset_layout.addWidget(self.offset_label)
        self.offset_field = QSpinBox()
        self.offset_field.setMinimum(-10000000)
        self.offset_field.setMaximum(10000000)
        self.offset_layout.addWidget(self.offset_field, 1)
        self.offset_layout.addStretch(2)
        self.main_layout.addLayout(self.offset_layout)

    def _create_slider(self):
        self.slider_layout = QHBoxLayout()

        self.slider_label = QLabel("0")
        self.slider_layout.addWidget(self.slider_label)

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setRange(0, 0)
        self.slider.setDisabled(False)
        self.slider_layout.addWidget(self.slider)

        self.main_layout.addLayout(self.slider_layout)

    def _create_render_button(self):
        self.render_layout = QHBoxLayout()
        self.render_button = QPushButton("Render")
        self.render_button.setDisabled(True)
        self.render_layout.addWidget(self.render_button)
        self.render_label = QLabel("Destination: ")
        self.render_layout.addWidget(self.render_label)
        self.render_lineedit = QLineEdit()
        self.render_layout.addWidget(self.render_lineedit)
        self.render_file_button = QPushButton("Select file")
        self.render_layout.addWidget(self.render_file_button)

        self.main_layout.addLayout(self.render_layout)
