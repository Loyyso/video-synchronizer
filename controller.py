import os
import sys
import time
from functools import partial

import ffmpeg
from PyQt5.QtCore import QUrl, QDir, QObject, pyqtSignal, QThread
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QLabel, QSlider

from model import AppModel
from view import AppView


class AppController:
    def __init__(self, view: AppView, model: AppModel):
        self._view = view
        self._model = model
        self._connect_signals()
        self._update_offset(self._model.offset)
        self.play_thread = None

    def _connect_signals(self):
        self._view.open_button_1.clicked.connect(partial(self._open_file, 1))
        self._view.open_button_2.clicked.connect(partial(self._open_file, 2))
        self._view.playButton.clicked.connect(self._play)

        self._view.offset_field.valueChanged.connect(self._update_offset)
        self._view.media_player_1.positionChanged.connect(self._position_changed)
        self._view.media_player_2.positionChanged.connect(self._position_changed)
        self._view.media_player_1.durationChanged.connect(partial(self.duration_changed, 1))
        self._view.media_player_2.durationChanged.connect(partial(self.duration_changed, 2))

        self._view.slider.sliderReleased.connect(self._slider_changed)
        self._view.render_button.clicked.connect(self._render)
        self._view.render_file_button.clicked.connect(self._select_destination)
        self._view.render_lineedit.textEdited.connect(self._destination_changed)

    def _open_file(self, btn_id):
        print("_open_file")
        filename = QFileDialog().getOpenFileName(self._view, "Open file", QDir.homePath(),
                                                 "Videos (*.mp4 *.webm *.mpeg *.mov *mkv *.avi)")[0]
        if filename != '':
            print(filename)
            if btn_id == 1:
                self._view.media_player_1.setMedia(
                    QMediaContent(QUrl.fromLocalFile(filename)))
                self._view.file1_label.setText("File 1: " + filename)
                self._model.file1 = filename
                self._view.media_player_1.play()
                self._view.media_player_1.pause()
                self._model.video1_duration = self._view.media_player_1.duration()
            else:
                self._view.media_player_2.setMedia(
                    QMediaContent(QUrl.fromLocalFile(filename)))
                self._view.file2_label.setText("File 2: " + filename)
                self._model.file2 = filename
                self._view.media_player_2.play()
                self._view.media_player_2.pause()
                self._model.video2_duration = self._view.media_player_2.duration()

    def _play(self):
        print("_play")
        if self._view.media_player_1.media().isNull() or self._view.media_player_2.media().isNull():
            return

        class PlayWorker(QObject):
            finished = pyqtSignal()

            def __init__(self, model: AppModel, media_player1: QMediaPlayer, media_player2: QMediaPlayer,
                         slider: QSlider, slider_label: QLabel):
                super().__init__()
                self.mp1 = media_player1
                self.mp2 = media_player2
                self.slider = slider
                self.slider_label = slider_label
                self.model = model

            def run(self):
                # playing videos
                while self.model.playing:
                    if self.model.video1_should_play() and self.mp1.state() == QMediaPlayer.PausedState:
                        self.mp1.play()
                    elif not self.model.video1_should_play() and self.mp1.state() != QMediaPlayer.PausedState:
                        self.mp1.pause()

                    if self.model.video2_should_play() and self.mp2.state() == QMediaPlayer.PausedState:
                        self.mp2.play()
                    elif not self.model.video2_should_play() and self.mp2.state() != QMediaPlayer.PausedState:
                        self.mp2.pause()

                    self.update_position()

                # pause
                self.mp1.pause()
                self.mp2.pause()
                self.finished.emit()

            def update_position(self):
                remaining_offset = 0
                if self.model.offset != 0:
                    remaining_offset = self.model.offset - self.mp1.position() \
                        if self.model.offset > 0 else -(self.model.offset + self.mp2.position())
                if remaining_offset > 0:
                    self.model.position = self.mp1.position() \
                        if self.model.offset > 0 else self.mp2.position()
                else:
                    if self.model.offset > 0:
                        self.model.position = max(self.mp1.position(),
                                                  self.mp2.position() + self.model.offset)
                    else:
                        self.model.position = max(self.mp1.position() + self.model.offset,
                                                  self.mp2.position())

        if not self._model.playing:
            self._model.playing = True
            self.play_thread = QThread()
            self.play_worker = PlayWorker(self._model, self._view.media_player_1, self._view.media_player_2,
                                          self._view.slider, self._view.slider_label)
            self.play_worker.moveToThread(self.play_thread)
            self.play_thread.started.connect(self.play_worker.run)
            self.play_worker.finished.connect(self.play_thread.quit)
            self.play_worker.finished.connect(self.play_worker.deleteLater)
            self.play_thread.finished.connect(self.play_thread.deleteLater)
            self.play_thread.start()
        else:
            self._model.playing = False

    def update_slider(self):
        self._model.video1_duration = self._view.media_player_1.duration()
        self._model.video2_duration = self._view.media_player_2.duration()
        self._view.slider.setRange(0, self._model.video_length())

    def _update_offset(self, value):
        print("_update_offset")
        self._model.offset = value
        self._view.media_player_1.setPosition(0)
        self._view.media_player_2.setPosition(0)
        self.update_slider()

    def _position_changed(self):
        if self._model.playing:
            self._view.slider.setValue(self._model.position)
        self._view.slider_label.setText(time.strftime('%H:%M:%S', time.gmtime(self._model.position / 1000)))

    def _slider_changed(self):
        print("_slider_changed")
        value = self._view.slider.value()

        if self._model.offset == 0:
            self._view.media_player_1.setPosition(min(self._view.media_player_1.duration(), value))
            self._view.media_player_2.setPosition(min(self._view.media_player_2.duration(), value))
        elif self._model.offset > 0:
            self._view.media_player_1.setPosition(min(self._view.media_player_1.duration(), value))
            self._view.media_player_2.setPosition(min(value - self._model.offset, self._view.media_player_2.duration()))
        elif self._model.offset < 0:
            self._view.media_player_1.setPosition(min(value + self._model.offset, value))
            self._view.media_player_2.setPosition(min(self._view.media_player_2.duration(), value))

        if self._model.playing:
            if self._view.media_player_1.position() < self._view.media_player_1.duration():
                self._view.media_player_1.play()
            if self._view.media_player_2.position() < self._view.media_player_2.duration():
                self._view.media_player_2.play()

    def _select_destination(self):
        print("_select_destination")
        options = QFileDialog.Options()
        options |= QFileDialog.AcceptSave
        filename = QFileDialog().getSaveFileName(self._view, QDir.homePath(), filter="Videos (*.mp4)", options=options)
        filename, extension = os.path.splitext(filename[0])
        filename = filename + ".mp4"

        if filename != '':
            self._view.render_lineedit.setText(filename)
            self._view.render_button.setDisabled(False)

    def _destination_changed(self):
        self._view.render_button.setDisabled(
            self._model.render_in_progress or not self._view.render_lineedit.text() != "")

    def duration_changed(self, duration, media_player):
        if media_player == 1:
            self._model.video1_duration = duration
        else:
            self._model.video2_duration = duration
        self.update_slider()

    def _render(self):
        if not os.path.isdir(os.path.dirname(self._view.render_lineedit.text())):
            QMessageBox.critical(self._view, "Error", "Destination directory does not exist.")
            return
        if os.path.splitext(self._view.render_lineedit.text())[1] != ".mp4":
            QMessageBox.critical(self._view, "Error", "Please select a .mp4 file.")
            return

        if self._view.media_player_1.media().isNull() or self._view.media_player_2.media().isNull():
            QMessageBox.critical(self._view, "Error", "Please select two videos.")
            return

        class FfmpegWorker(QObject):
            finished = pyqtSignal()

            def __init__(self, filename, offset, file1, file2):
                super().__init__()
                self.filename = filename
                self.offset = offset
                self.file1 = file1
                self.file2 = file2

            def run(self):
                if os.path.isfile(self.filename):
                    os.remove(self.filename)

                print(self.file1)
                in_file = ffmpeg.input(self.file1)
                v1 = in_file.filter("fps", fps=60).filter("tpad", start_mode="add",
                                                          stop_mode="add", start_duration=str(
                        (-self.offset if self.offset < 0 else 0)) + "ms")
                a1 = in_file.audio.filter("adelay", str(-self.offset if self.offset < 0 else 0) + "ms", all="true")

                print(self.file2)
                in_file_2 = ffmpeg.input(self.file2)
                v2 = in_file_2.filter("fps", fps=60).filter("tpad", start_mode="add",
                                                            stop_mode="add", start_duration=str(
                        (self.offset if self.offset > 0 else 0)) + "ms")
                a2 = in_file_2.audio.filter("adelay", str(self.offset if self.offset > 0 else 0) + "ms", all="true")

                audio = ffmpeg.filter((a1, a2), "amix", inputs=2)  # weights for both volumes can be added with amix
                video = ffmpeg.filter((v1, v2), "xstack", inputs=2, layout="0_0|0_h0", fill="#000000")

                if sys.platform == "win32":
                    for path in os.get_exec_path():
                        if os.path.isfile(os.path.join(path, "ffmpeg.exe")):
                            print("ffmpeg found in ", path)
                            ffmpeg_path = os.path.join(path, "ffmpeg.exe")

                elif sys.platform.startswith(prefix="linux"):
                    for path in os.get_exec_path():
                        if os.path.isfile(os.path.join(path, "ffmpeg.exe")):
                            print("ffmpeg found in ", path)
                            ffmpeg_path = os.path.join(path, "ffmpeg.exe")
                else:
                    if sys.platform == "win32":
                        if hasattr(sys, '_MEIPASS'):
                            ffmpeg_path = os.path.join(sys._MEIPASS, "ffmpeg.exe")
                        else:
                            ffmpeg_path = os.path.join(os.path.abspath("."), "ffmpeg", "ffmpeg.exe")
                    elif sys.platform.startswith(prefix="linux"):
                        if hasattr(sys, '_MEIPASS'):
                            ffmpeg_path = os.path.join(sys._MEIPASS, "ffmpeg")
                        else:
                            ffmpeg_path = os.path.join(os.path.abspath("."), "ffmpeg", "ffmpeg")

                print(ffmpeg_path)
                ffmpeg.output(video, audio, self.filename).run(cmd=ffmpeg_path)

                self.finished.emit()

        self.thread = QThread()
        self.ffmpeg_worker = FfmpegWorker(self._view.render_lineedit.text(), self._model.offset, self._model.file1,
                                          self._model.file2)
        self.ffmpeg_worker.moveToThread(self.thread)
        self.thread.started.connect(self.ffmpeg_worker.run)
        self.ffmpeg_worker.finished.connect(self.thread.quit)
        self.ffmpeg_worker.finished.connect(self.ffmpeg_worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.thread.finished.connect(lambda: QMessageBox.information(self._view, "Info", "Render finished"))

        self._view.render_button.setDisabled(True)
        self._view.render_lineedit.setDisabled(True)
        self._view.render_file_button.setDisabled(True)
        self._model.render_in_progress = True
        self.thread.finished.connect(lambda: self._view.render_button.setDisabled(False))
        self.thread.finished.connect(lambda: self._view.render_lineedit.setDisabled(False))
        self.thread.finished.connect(lambda: self._view.render_file_button.setDisabled(False))

        self.thread.start()
