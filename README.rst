Video Synchronizer
==================

Requirements
------------

On Windows, you will need to install `LAVFilters
<https://github.com/Nevcairiel/LAVFilters/releases>`_. You can also install
`K-lite Codec Pack
<https://www.codecguide.com/download_k-lite_codec_pack_basic.htm>`_ instead.

Setup environment
-----------------

Using poetry, run

.. code:: bash

   poetry install

to setup the environment.

Build
-----

You will need to install pyinstaller.

**For the following commands, replace the semicolon in "--add-data" with a
colon if you are building on Linux.**

With ffmpeg
~~~~~~~~~~~

Run

.. code:: bash

   pyinstaller --add-data ffmpeg/*;. --add-data icon.ico;. -y --onefile --windowed --icon="icon.ico" --name VideoSynchronizer main.py

to build the application and include ffmpeg.

You will need to add ffmpeg binaries and libraries in a directory named
``ffmpeg``, located in the project root.


Without ffmpeg
~~~~~~~~~~~~~~

In this case, you will need to put ffmpeg in ``PATH`` to use the application.

Run

.. code:: bash

   pyinstaller --add-data icon.ico;. -y --onefile --windowed --icon="icon.ico" --name VideoSynchronizer main.py

to build the application without ffmpeg.
