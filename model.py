class AppModel:
    def __init__(self):
        self.playing = False
        self.offset = 0
        self.render_in_progress = False
        self.video1_duration = 0
        self.video2_duration = 0
        self.position = 0
        self.file1 = ""
        self.file2 = ""

    def video_length(self):
        if self.offset > 0:
            video_length = max(self.video1_duration,
                               self.video2_duration + self.offset)
        elif self.offset < 0:
            video_length = max(self.video1_duration - self.offset,
                               self.video2_duration)
        else:
            video_length = max(self.video1_duration,
                               self.video2_duration)
        return video_length

    def video1_should_play(self):
        if self.offset >= 0:
            return self.position <= self.video1_duration
        else:
            return 0 <= self.position + self.offset <= self.video1_duration

    def video2_should_play(self):
        if self.offset <= 0:
            return self.position <= self.video2_duration
        else:
            return 0 <= self.position - self.offset <= self.video2_duration
